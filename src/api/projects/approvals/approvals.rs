// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Query for project approval configuration.
/// https://docs.gitlab.com/ee/api/merge_request_approvals.html#project-level-mr-approvals
#[derive(Debug, Builder, Clone)]
pub struct ProjectApprovals<'a> {
    /// The project to query for approval configuration.
    #[builder(setter(into))]
    project: NameOrId<'a>,
}

impl<'a> ProjectApprovals<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> ProjectApprovalsBuilder<'a> {
        ProjectApprovalsBuilder::default()
    }
}

impl<'a> Endpoint for ProjectApprovals<'a> {
    fn method(&self) -> Method {
        Method::GET
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/approvals", self.project).into()
    }
}

#[cfg(test)]
mod tests {
    use crate::api::projects::approvals::{ProjectApprovals, ProjectApprovalsBuilderError};
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn project_is_necessary() {
        let err = ProjectApprovals::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, ProjectApprovalsBuilderError, "project");
    }

    #[test]
    fn project_is_sufficient() {
        ProjectApprovals::builder().project(1).build().unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("projects/simple%2Fproject/approvals")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = ProjectApprovals::builder()
            .project("simple/project")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
