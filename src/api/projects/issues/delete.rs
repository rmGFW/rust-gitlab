// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Delete an issue on a project.
#[derive(Debug, Builder, Clone)]
pub struct DeleteIssue<'a> {
    /// The project the issue belongs to.
    #[builder(setter(into))]
    project: NameOrId<'a>,
    /// The ID of the issue.
    issue: u64,
}

impl<'a> DeleteIssue<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> DeleteIssueBuilder<'a> {
        DeleteIssueBuilder::default()
    }
}

impl<'a> Endpoint for DeleteIssue<'a> {
    fn method(&self) -> Method {
        Method::DELETE
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/issues/{}", self.project, self.issue).into()
    }
}

#[cfg(test)]
mod tests {
    use http::Method;

    use crate::api::projects::issues::{DeleteIssue, DeleteIssueBuilderError};
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn project_and_issue_are_necessary() {
        let err = DeleteIssue::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, DeleteIssueBuilderError, "project");
    }

    #[test]
    fn project_is_necessary() {
        let err = DeleteIssue::builder().issue(1).build().unwrap_err();
        crate::test::assert_missing_field!(err, DeleteIssueBuilderError, "project");
    }

    #[test]
    fn issue_is_necessary() {
        let err = DeleteIssue::builder().project(1).build().unwrap_err();
        crate::test::assert_missing_field!(err, DeleteIssueBuilderError, "issue");
    }

    #[test]
    fn project_and_issue_are_sufficient() {
        DeleteIssue::builder().project(1).issue(1).build().unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::DELETE)
            .endpoint("projects/simple%2Fproject/issues/1")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = DeleteIssue::builder()
            .project("simple/project")
            .issue(1)
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
