// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Delete an issue note on a project.
#[derive(Debug, Builder, Clone)]
pub struct DeleteIssueNote<'a> {
    /// The project the issue belongs to.
    #[builder(setter(into))]
    project: NameOrId<'a>,
    /// The ID of the issue.
    issue: u64,
    /// The ID of the note.
    note: u64,
}

impl<'a> DeleteIssueNote<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> DeleteIssueNoteBuilder<'a> {
        DeleteIssueNoteBuilder::default()
    }
}

impl<'a> Endpoint for DeleteIssueNote<'a> {
    fn method(&self) -> Method {
        Method::DELETE
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!(
            "projects/{}/issues/{}/notes/{}",
            self.project, self.issue, self.note,
        )
        .into()
    }
}

#[cfg(test)]
mod tests {
    use http::Method;

    use crate::api::projects::issues::notes::{DeleteIssueNote, DeleteIssueNoteBuilderError};
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn project_issue_and_note_are_necessary() {
        let err = DeleteIssueNote::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, DeleteIssueNoteBuilderError, "project");
    }

    #[test]
    fn project_is_necessary() {
        let err = DeleteIssueNote::builder()
            .issue(1)
            .note(1)
            .build()
            .unwrap_err();
        crate::test::assert_missing_field!(err, DeleteIssueNoteBuilderError, "project");
    }

    #[test]
    fn issue_is_necessary() {
        let err = DeleteIssueNote::builder()
            .project(1)
            .note(1)
            .build()
            .unwrap_err();
        crate::test::assert_missing_field!(err, DeleteIssueNoteBuilderError, "issue");
    }

    #[test]
    fn note_is_necessary() {
        let err = DeleteIssueNote::builder()
            .project(1)
            .issue(1)
            .build()
            .unwrap_err();
        crate::test::assert_missing_field!(err, DeleteIssueNoteBuilderError, "note");
    }

    #[test]
    fn project_issue_and_note_are_sufficient() {
        DeleteIssueNote::builder()
            .project(1)
            .issue(1)
            .note(1)
            .build()
            .unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::DELETE)
            .endpoint("projects/simple%2Fproject/issues/1/notes/1")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = DeleteIssueNote::builder()
            .project("simple/project")
            .issue(1)
            .note(1)
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
